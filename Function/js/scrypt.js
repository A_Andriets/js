//Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.

function map(fn, array) { 
    let res = [];
    for (let i = 0; i < array.length; i++) { 
        res = fn(array[i]) + '<br>';
        document.write(res);

    }
}
let fn = function (y) {
    return y*y;
}
array = [1, 6, 9];
map(fn, array);

map(fn, [55]);
map(fn, ["hard"]);



//Використовуючи CallBack function, створіть калькулятор, який буде від користувача приймати 2 числа і знак арифметичної операції. При введенні не числа або при розподілі на 0 виводити помилку.

document.write(`<hr color="blue">`)

const x = parseInt(prompt('Введіть число', '0'));
document.write(`<p> ${x} </p>`);
const y = parseInt(prompt('Введіть число', '0'));
document.write(`<p> ${y} </p>`);
const sign = prompt('Введіть знак арифметичної операції', '*');
document.write(`<p> ${sign} </p>`);
var result = 0;

const sum = function() { 
    result = x + y;
    document.write(`Результат складення ${x} + ${y} = ${result} <br>`);
}
const subtraction = function() { 
    result = x - y;
    document.write(`Результат віднімання ${x} - ${y} = ${result} <br>`);
}
const divide = function() { 
    result = x / y;
    document.write(`Результат ділення ${x} / ${y} = ${result} <br>`);
}
const multiply = function() { 
    result = x * y;
    document.write(`Результат множення ${x} * ${y} = ${result} <br>`);
}
const notNumber = function() { 
        console.error("На нуль ділити не можна");
}

function calc(callback) { 
    if (isNaN(x) || isNaN(y) || y === 0) {  // Чому, якщо вказую умову x === NaN або x !== Number - не спрацьовує код і виводе помилку 'Nan is not defined'. Як ще можна задати умову, що НЕ число?
        return notNumber();
    } else if (sign === '+') {
        document.write(sum() + '<br>');
    } else if (sign === '-') {
        return subtraction();
    } else if (sign === '/') {
        return divide();
    } else if (sign === '*') {
        multiply();
    }
    document.write(result + "<br>");
}
calc();
document.write(result);



