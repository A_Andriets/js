/*
Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/
class Worker { 
    constructor(name, surname, rate, days) { 
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }
    getSalary() { 
        return this.rate * this.days;
    }
}

let worker = new Worker("Andrew", "Statham", 100, 5);
document.write(worker.getSalary());
worker = new Worker("Ivan", "Statham", 30, 5);
console.log(worker);
document.write("<br />" + worker.getSalary() + '<hr/>');

/*
Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.
*/

class MyString { 
    constructor(str) { 
        this.str = str;
    }
    reverse() { 
        return this.str.split("").reverse().join("");
    }
    ucFirst() { 
        return this.str[0].toUpperCase() + this.str.slice(1);
    }
    ucWords() {
            
            let sentense = this.str.split(" ");
                for (let i = 0; i < sentense.length; i++) {
                sentense[i] = sentense[i][0].toUpperCase() + sentense[i].substr(1);
                }
                   
        /*    
        for (let word of sentense) { 
            sentense[word] = word.charAt(0).toUpperCase() + word.substr(1);
            }
        */
        //Підкажіть, що не так з цим перебором? Чому не спрацьовує?
            return sentense.join(" * ");
        }
    }


let myString = new MyString(prompt('Enter string', "String"));
document.write(myString.reverse() + "<br/>");
console.log(myString)
document.write(`${ myString.ucFirst() } <br/>`);
document.write(myString.ucWords("kelo ti") + "<br/>");

/*
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.

*/

class Phone { 
    constructor(number, model, weight) { 
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    receiveCall(name) { 
        console.log(`Телефонує ${name}`);
    }
    getNumber() { 
        return console.log(this.number);
    }
}

let phone1 = new Phone( "+3809454654", "Iphone 7", "150g");
let phone2 = new Phone("Xiaomi", "Redni", "120g");
let phone3 = new Phone("Nokia", 8, "200g");

console.log(phone1.number);
console.log(phone2.weight);
console.log(phone2);
alert(phone2.weight);
console.log(phone1);
console.log(phone3);

phone1.receiveCall("Ivan");
phone1.getNumber();

/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

*/

class Car { 
    constructor(brand, carClass, weight, driver, engine) {
        this.brand = brand;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;

    }
    start() { 
        console.log("Поїхали");
    };
    stop() { 
        console.log("Зупиняємося");
    };
    turnRight() { 
        console.log("Поворот праворуч");
    };
    turnLeft() {
        console.log("Поворот ліворуч");
     };
    toString() { 
        document.write("<br /> CAR: <br/>");
        document.write(`Бренд: ${this.brand} <br/>`);
        document.write(`Клас авто: ${this.carClass} <br/>`);
        document.write(`Вага: ${this.weight} <br/>`);
        document.write(`<br />Driver:<br />`);
        document.write(`ПІБ: ${driver["fullName"]} <br/>`);
        document.write(`Стаж водіння: ${driver["experience"]} <br/>`);
        document.write(`<br/> Engine: <br/>`);
        document.write(`Потужність: ${engine["power"]} <br/>`);
        document.write(`Виробник: ${engine.producer} <br/>`);

    };

}

class Engine { 
    constructor(power, producer) {
        this.power = power;
        this.producer = producer;        
     }

}

class Driver { 
    constructor(fullName, experience) {
        this.fullName = fullName;
        this.experience = experience;  
     }

}

let engine = new Engine(260, "kia");
let driver = new Driver("Pupkin", 25);
let car = new Car("Kia", "Bussines", 1500, driver, engine);


car.start();
car.stop();
car.turnRight();
car.turnLeft();
car.toString();
console.log(car);
console.log(engine);

/*
Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

class Lorry { 
    constructor(liftingCapacity) { 
        this.liftingCapacity = liftingCapacity;
    }
};

class SportCar { 
    constructor(maxSpeed) { 
        this.maxSpeed = maxSpeed;
    }
};


Lorry.prototype = car;
SportCar.prototype = car;

let lorry = new Lorry("50 тон");
let sportCar = new SportCar("550 m/h");

document.write(lorry.liftingCapacity);

document.write("<hr/>");

function Cars(brand, carClass, weight, drivers, engines) {
        this.brand = brand;
        this.carClass = carClass;
        this.weight = weight;
        this.drivers = drivers;
        this.engines = engines;
        this.toString = function() { 
            document.write("<br /> CAR: <br/>");
            document.write(`Бренд: ${this.brand} <br/>`);
            document.write(`Клас авто: ${this.carClass} <br/>`);
            document.write(`Вага: ${this.weight} <br/>`);
            document.write(`<br />Driver:<br />`);
            document.write(`ПІБ: ${drivers["fullName"]} <br/>`);
            document.write(`Стаж водіння: ${drivers["experience"]} <br/>`);
            document.write(`<br/> Engine: <br/>`);
            document.write(`Потужність: ${engines["power"]} <br/>`);
            document.write(`Виробник: ${engines.producer} <br/>`);
            console.log(`Вантажопідйомність кузова: ${this.liftingCapacity}`);
            if (this.maxSpeed === undefined) { }
            else {
                console.log("Максимальна швидкість:" + this.maxSpeed);
                document.write("Максимальна швидкість:" + this.maxSpeed);
            };
            if (this.liftingCapacity === undefined) { } else {document.write(`Вантажопідйомність кузова: ${this.liftingCapacity} <br/>`);}

        };
}

function Engines(power, producer) {
        this.power = power;
        this.producer = producer;        
}


function Drivers(fullName, experience) {
        this.fullName = fullName;
        this.experience = experience;  
}

let engines = new Engines(220, "mers");
let drivers = new Drivers("Ivanov", 45);
let cars = new Cars("Mercedess", "coupe", 2000, drivers, engines);

console.log(cars);
console.log(engines);
console.log(drivers);



let engine2 = new Engines(300, "hiu");
let driver2 = new Drivers("Vasechkin", 70);
let car2 = new Cars("Hiunday", "jeep", 3000, driver2, engine2);

function Lorrys(liftingCapacity) { 
        this.liftingCapacity = liftingCapacity;
};

function SportCars(maxSpeed) { 
        this.maxSpeed = maxSpeed;
};

let car3 = new Cars("Daf", "lorry", 2000, driver2, engine2);

Lorrys.prototype = car2;
SportCars.prototype = car3;

let lorrys = new Lorrys("100 тон");
let sportCars = new SportCars("950 mile/hour");


lorrys.toString();
sportCars.toString();


/*
Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.

*/


function Animal(food, location) { 
    this.food = food;
    this.location = location;
    this.makeNoise = function () { 
        console.log("Така тварина спить");
    }
    this.eat = function () { 
        console.log(`Eat ${this.food}`);
    }
    this.sleep = function () { 
        console.log(`Sleep ${this.location}`);
    }
}

function Dog(name, bark) { 
    this.name = name;
    this.bark = bark;
    this.makeNoise = function () { 
        console.log("Така тварина гафкає");
    }
    this.eat = function () { 
        console.log(`Eat meat`);
    }
}

function Cat(name, mew) { 
    this.name = name;
    this.mew = mew;
    this.makeNoise = function () { 
        console.log("Така тварина мявкає");
    }
    this.eat = function () { 
        console.log(`Eat milk`);
    }
}

function Horse(name, neighs) { 
    this.name = name;
    this.neighs = neighs;
    this.makeNoise = function () { 
        console.log("Така тварина ірже");
    }
    this.eat = function () { 
        console.log(`Eat grass`);
    }
}




let animal = new Animal("їжа", "HZ");
animal.makeNoise();

Dog.prototype = animal;
Cat.prototype = animal;
Horse.prototype = animal;

let dog = new Dog("Tayson", "gaf-gaf");
console.log(dog.name);
dog.makeNoise();
dog.eat();

let cat = new Cat("Murka", "mew-mew");
cat.makeNoise();
cat.eat();

let horse = new Horse("Zorka", "phhh");
horse.makeNoise();
horse.eat();

/*
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.
*/


document.write("<hr/>");

function Vet(animal) { 
    this.treatAnimal = function() { 
        document.write(`<br/> Food: ${animal["food"]} <br/>`);
        document.write(`<br/> Location: ${animal.location} <br/>`);
    }
    this.main = function () { 
        let animal = [dog, cat, horse];
        for (let i = 0; i < animal.length; i++) { 
            document.write("Записати на прийом:" + animal[i].name + "<br/>");
        }

    }
}

let vet = new Vet(animal);
vet.treatAnimal();
vet.main();