/*
Створіть програму секундомір.
            <br>
            Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
            При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
            Виведення лічильників у форматі ЧЧ:ММ:СС<br>
            Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/



    //timer function
    let counter = 0;
    const hour = document.querySelector("#hour");
    const min = document.querySelector("#min");
    const sec = document.querySelector("#sec");
    const startbtn = document.querySelector("#startbtn");
    const stopbtn = document.querySelector("#stopbtn");
    const resetbtn = document.querySelector("#resetbtn");
    const display = document.querySelector(".stopwatch-display");
    let handler;

    var timer = () => {
        // counter++
        sec.textContent = +sec.textContent + 1;
        if (sec.textContent < 10) { 
            sec.textContent = "0" + sec.textContent;
        }
        if (sec.textContent >= 59) {
            // counter = 0;
            // counter++;
            min.textContent = +min.textContent + 1;
            if (min.textContent < 10) {
                min.textContent = '0' + min.textContent;
            }
  
            sec.textContent = '00'
        }
        if (min.textContent >= 59) { 
            // counter = 0;
            // counter++;
            hour.textContent = +hour.textContent + 1;
            if (hour.textContent < 10) { 
                hour.textContent = '0' + hour.textContent;
            }
            min.textContent = '00'
        }  
    }
const ban = button => button.disabled = true;
const allow = button => button.disabled = false;
        
startbtn.onclick = () => { 
    handler = setInterval(timer, 100);
    display.style.background = "green";
    ban(startbtn);
    allow(stopbtn);
    allow(resetbtn);
}    

stopbtn.onclick = () => { 
    clearInterval(handler);
    display.style.background = "red";
    allow(startbtn);
    allow(resetbtn);
}

resetbtn.onclick = () => { 
    clearInterval(handler);
    display.style.background = "silver";
    allow(startbtn);
    hour.textContent = '00';
    min.textContent = '00';
    sec.textContent = '00';
}
