/*
    Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body":
*/


window.addEventListener('DOMContentLoaded', function () { 

    // create HttpRequest
    const request = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/comments";
    request.open('GET', url);
    show()
    request.send();
    request.addEventListener('readystatechange', () => { 
        if (request.readyState == 4 && request.status >= 200 && request.status < 300) {
            console.log(JSON.parse(request.responseText))
            createComments(JSON.parse(request.responseText))
            setTimeout(() => hide(), 2000)

            // document.getElementById("output").innerHTML = xhr.responseText;
        } else if (request.readyState == 4) {
            throw new Error("Помилка")
        }
    })
})

// test create object 
let currentPage = 0;
function createComments(data) { 
    if(!Array.isArray(data)){
        throw new Error("З сервера прийшов не масив!");
    }
    const newData = data.map(obj => { 
        // const div = document.createElement('div');
        // document.querySelector('.comments').after(div)
        
        // div.insertAdjacentHTML('beforeend', `<div>№: ${obj.id}</div>
        // <div>Заголовок: ${obj.name}</div>
        // <div>E-mail: ${obj.email}</div>
        // <div>${obj.body}</div>`)
        return obj;

    })
    // const newArr = newData.slice(currentPage * 10, (currentPage * 10) + 10).reverse().map(item => {
    //     const div = document.createElement('div');
    //     document.querySelector('.comments').after(div)
    //     div.insertAdjacentHTML('beforeend', `<div><span class="com">№:</span> ${item.id}</div>
    //     <div><span class="com">Заголовок:</span> ${item.name}</div>
    //     <div><span class="com">E-mail:</span> ${item.email}</div>
    //     <div>${item.body}</div>`)
    //     div.classList.add("div-wrapper")
    //     return item;
    // });
    // console.log(newArr)
    nw(newData);
    pagination(newData);
}
const main = document.createElement('section');
document.querySelector('.comments').after(main);
// const div = document.createElement('div');


// create pagination
function pagination(newData) { 
    const [...paginationBtn] = document.querySelectorAll('#Pag_btn');
    console.log(paginationBtn)
    paginationBtn.forEach(el =>
    el.addEventListener('click', (el) => { 
        console.log(el.target.innerText)
        if (el.target.innerText == 'Previous' && currentPage != 0) { 
            currentPage--;
            main.innerHTML = ''
            nw(newData);
        }
        if (el.target.innerText == 'Next' && (Math.round(newData.length / currentPage)) != 10) { 
            console.log(currentPage)
            currentPage++;
            main.innerHTML = ''
            nw(newData);
        }
}))
}

// create comments from object
function nw(newData) { 
    const newArr = newData.slice(currentPage * 10, (currentPage * 10) + 10).reverse().map(item => {
        const div = document.createElement('div');
        const btn = document.createElement("button");
        // document.querySelector('.comments').after(div);
        main.prepend(div)
        div.insertAdjacentHTML('beforeend', `<div><span class="com">№:</span> ${item.id}</div>
        <div><span class="com">Заголовок:</span> ${item.name}</div>
        <div><span class="com">E-mail:</span> ${item.email}</div>
        <div class="div-com">${item.body}</div>`);
        div.classList.add("div-wrapper");
        div.append(btn);
        btn.innerText = "Відповісти";
        btn.classList.add('btn')

    return item;
    });
    
}

function hide() { 
    document.querySelector('.modal-loader').classList.remove('active');
}

function show() { 
    document.querySelector('.modal-loader').classList.add('active');
}

