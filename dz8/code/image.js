/*
Створіть картинку та кнопку з назвою "Змінити картинку"
зробіть так щоб при завантаженні сторінки була картинка
https://itproger.com/img/courses/1476977240.jpg
При натисканні на кнопку вперше картинка замінилася на
https://itproger.com/img/courses/1476977488.jpg
при другому натисканні щоб картинка замінилася на
https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png* Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав
*/


window.onload = () => {
    
    const change = document.getElementById("change")
 
    var i = 0;
    var imgs = new Array("https://itproger.com/img/courses/1476977240.jpg", "https://itproger.com/img/courses/1476977488.jpg", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png")

    
    change.onclick = () => {
        var img = document.querySelector(".image");
        if (i < imgs.length - 1) {
            i++;
            img.src = imgs[i]
        }
       

        
    }

}