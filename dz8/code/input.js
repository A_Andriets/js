/*
Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.
*/
    document.querySelector(".submit").onclick = () => { 
    const [...input] = document.getElementsByClassName("name");
    input[1].after(input[0]);
}

/* Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі. Багаторядкове поле слід очистити після переміщення інформації.
*/
    const div = document.createElement("div");
    const text = document.getElementById("textarea");
   

    document.getElementsByTagName("input")[3].onclick = () => { 
        div.textContent = text.value;
        text.before(div);
        text.remove();
}
    
/*
 Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.
 */
    const [...divs] = document.getElementsByTagName("div")
    divs.forEach(color => {
        color.style.backgroundColor = "blue";
    })

/*
Створіть картинку та кнопку з назвою "Змінити картинку"
зробіть так щоб при завантаженні сторінки була картинка
https://itproger.com/img/courses/1476977240.jpg
При натисканні на кнопку вперше картинка замінилася на
https://itproger.com/img/courses/1476977488.jpg
при другому натисканні щоб картинка замінилася на
https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png
*/

window.onload = () => {
    
    const change = document.getElementById("change")
    var i = 0;
    var imgs = new Array("https://itproger.com/img/courses/1476977240.jpg", "https://itproger.com/img/courses/1476977488.jpg", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png")
    
    change.onclick = () => {
        var img = document.querySelector(".image");
        if (i < imgs.length - 1) {
            i++;
            img.src = imgs[i]
        }
    }
}

/*
Створіть на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав
*/
    const p = document.querySelectorAll(".p");  
        p.forEach(paragraph => {
            paragraph.onclick = () => { 
                paragraph.remove();
            }
        });
