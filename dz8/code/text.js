/* Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі. Багаторядкове поле слід очистити після переміщення інформації.
*/

window.onload = () => { 
    const div = document.createElement("div");
    const text = document.getElementById("textarea");


    document.querySelector("input").onclick = () => { 
        div.textContent = text.value;
        document.body.prepend(div);
        text.remove();
    }

}