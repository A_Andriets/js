/*
 Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.
 */

window.onload = () => {
    const [...divs] = document.getElementsByTagName("div")

    divs.forEach(color => {
        color.style.backgroundColor = "blue";
    })
}