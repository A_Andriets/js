const rest = JSON.parse(localStorage.restorationBD)
console.log(rest)

if (!Array.isArray(rest)) { 
    throw console.error('Тут має бути масив')
}

const restEl = rest.map(({ description, ingridients, productimageUrl, productName, price, keywords}) => {
    // console.log(productImage)
    return `
    <tbody>
            <tr>
                <td><img src="${productimageUrl}" alt="${keywords}"></td>
                <td>${productName}</td>
                <td>${description}</td>
                <td>${ingridients}</td>
                <td>${price} грн.</td>
            </tr>
        </tbody>
    `
})
 
console.log(restEl)
document.querySelector("table").
    insertAdjacentHTML('beforeend', restEl.join(""))


    /*
    <tbody>
            <tr>
                <td><img src="https://s1.eda.ru/StaticContent/Photos/120131082439/160124115932/p_O.jpg" alt=""></td>
                <td>Plov</td>
                <td>Dishes</td>
                <td>50$</td>
            </tr>
        </tbody>


            <div class="img">
            <img src="${productImage}" class="store_img">
    </div>
    <div class="product">
    <div class="header">
        <h2 class="img_header">${productName}</h2>
    </div>
    <p class="img_descr">${productDescription}</p>
    <div class="price">${porductPrice} грн. </div>
    
    </div>
    */