/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
    operand1 : "",
    sign : "",
    operand2 : "",
    rez : "",
    mem: ""
}
const equil = document.querySelector(".orange");
const point = document.querySelector("#point");
const memorySign = document.querySelector('.memory');
// https://regexr.com/
let memoryV = false;
const disabled = (btn) => btn.disabled = true;
const unable = (btn) => btn.disabled = false;

document.querySelector(".keys").addEventListener("click", (e) => {
    if(validate(/[\d.]/, e.target.value) && calculate.sign === ''){
        calculate.operand1 += e.target.value;
         // point in operand
         if (calculate.operand1.includes('.')) disabled(point)
        show(calculate.operand1);
        //check sign
    } else if (validate(/^[-+/*]$/, e.target.value)) {
        calculate.sign = e.target.value;
        unable(point)
        // calculate operations
    } else if (validate(/^=$/, e.target.value)) {
        unable(point);
        let a = +`${calculate.operand1}`;
        let b = +`${calculate.operand2}`;
        if (calculate.sign === '+') {
            calculate.rez = a + b;
            show(calculate.rez);
            clear();
        }
        else if (calculate.sign === '-') {
            calculate.rez = a - b;
            show(calculate.rez);
            clear();
        }
        else if (calculate.sign === '*') {
            calculate.rez = a * b;
            show(calculate.rez);
            clear();
        }
        else { 
            if (b === 0) {
                show("error");
                setTimeout(() => {
                    clear();
                    show("");
                }, 2000);
            } else { 
                calculate.rez = a / b;
                show(calculate.rez);
            }
        }
    }
    // repeal disabled
    if (calculate.operand1 && calculate.sign) { 
        unable(equil);
    }
    // record operand2
    if (calculate.operand1 !== '' && calculate.sign !== '' && /[\d.]/.test(e.target.value)) {
        calculate.operand2 += e.target.value;
        if (calculate.operand2.includes('.')) disabled(point)
        show(calculate.operand2);
    }
    memory(e);
    if (/^c$/gi.test(e.target.value) && e.target.value !== 'mrc') {
        clear();
        show('');
        calculate.rez = '';
        if (document.querySelector('.memory')) {
            document.querySelector('.memory').remove();
            calculate.mem = '';
            memoryV = false;
        }
    }

   
})

function show (v) {
    const d = document.querySelector(".display input");

    d.value = v;
}

const validate = (r, v) => r.test(v);



// clear display
function clear() { 
    calculate.operand1 = '';
    calculate.operand2 = '';
    calculate.sign = '';
}

function memory(e) {
    let m = +`${document.querySelector(".display input").value}`
    if (/[M+]{2}/gi.test(e.target.value)) {
        calculate.mem = +calculate.mem + m;
        document.querySelector(".display input").insertAdjacentHTML('beforebegin', `<div class="memory">m</div>`)
    } else if (/[M-]{2}/gi.test(e.target.value)) {
        calculate.mem = +calculate.mem - m;
        document.querySelector(".display input").insertAdjacentHTML('beforebegin', `<div class="memory">m</div>`)
    } else if (/[Mrc]{3}/gi.test(e.target.value) && memoryV === false) {
        show(calculate.mem);
        memoryV = true;
    } else if (/[Mrc]{3}/gi.test(e.target.value) && memoryV) { 
        show('0');
        calculate.mem = '';
        document.querySelector('.memory').remove();
        memoryV = false
    }
}
