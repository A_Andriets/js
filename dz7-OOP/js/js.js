/*
- Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*/




let arr = [];
let dataType = ''; 

function filterBy(element) {
    console.log(typeof element)
    return typeof element !== dataType;

}

arr = [1, Boolean, 'world', 23];
dataType = 'string';

let filtered = arr.filter(filterBy);
console.log(filtered);

arr = [11, Boolean, 'stop', 2659, 'monkey', 'number', 1569];
dataType = 'number';

let filter2 = arr.filter(filterBy);
console.log(filter2);

arr = [256, 'hello', 2563, 'duck', 1569, true, false];
dataType = 'boolean';

filter2 = arr.filter(filterBy);
console.log(filter2);
// document.write(filter2[2])

arr = ['hello', 'world', 23, '23', null];
dataType = 'string';

filter = arr.filter(filterBy);
console.log(filter);
// document.write(filter[0])