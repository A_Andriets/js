import { pizzaUser, pizzaBD } from "./data-pizza.js";

const table = document.querySelector(".table")

const pizzaSelectSize = (e) => {
    console.log(e)
    if(e.target.tagName === "INPUT" && e.target.checked){
        pizzaUser.size = pizzaBD.size.find(el=>el.name === e.target.id);
        /*
        pizzaBD.size.forEach((el)=>{
            if(e.target.id === el.name){
                console.log(el)
            }
        })
        */
    
    }
    show(pizzaUser)
};

const pizzaSelectTopping = (e) => {
    if (!e.type) e.target = e;
    console.log(e.type)
    console.log(e.target)
    const classS = document.getElementsByClassName("draggable sauce")
    console.log(classS)
    switch (e.target.id) {
        case "sauceClassic" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
        break;
        case "sauceBBQ" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
        break;
        case "sauceRikotta" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        
        default:
        // let el = pizzaBD.topping.find(el => el.name === e.target.id);
        if (pizzaUser.topping.length) {
            if (pizzaUser.topping.find(el => el === pizzaBD.topping.find(el => el.name === e.target.id))) {
                pizzaUser.topping.map(el => {
                    if (el.name === e.target.id) el.quantity++;
                })
            } else {
                let el = pizzaBD.topping.find(el => el.name === e.target.id);
                el.quantity = 1;
                pizzaUser.topping.push(el);
            }
        } else {
            pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
            pizzaUser.topping[0].quantity = 1;
        }
        break;
    }
    
        /*
        case "moc1" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "moc2" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "moc3" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "telya" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "vetch1" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
        break;
        case "vetch2" : pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
break;}
        */
    

    show(pizzaUser);

    if (e.target.tagName === "IMG") {
        if (e.target.id.match('sauce')) {
            const sauce = document.querySelector('.sauce-img')
            sauce.innerHTML = `<img src="${e.target.src}">`
        } else {table.insertAdjacentHTML("beforeend", `<img src="${e.target.src}" data-name="${e.target.id}">`)}
        
    }
}


function show (pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping")

    let totalPrice = 0;
    if(pizza.size !== ""){
        totalPrice += parseFloat(pizza.size.price);
    }
    if(pizza.sauce !== ""){
        totalPrice += parseFloat(pizza.sauce.price);
    }
    if(pizza.topping.length){
        totalPrice += pizza.topping.reduce((a,b)=>a + (b.price*b.quantity), 0)
    }
    price.innerText = totalPrice;
   
    if(pizza.sauce !== ""){
        sauce.innerHTML = `<span class="topping">${pizza.sauce.productName}<button class="del-sauce"></button></span>`
    }

    if (Array.isArray(pizza.topping)) {
        topping.innerHTML = pizza.topping.map(el => {
            if (el.quantity > 1) {
                return `<span class="topping" data-quantity=${el.quantity}>${el.productName}<button class="del"></button></span>`;
            } else { return `<span class="topping">${el.productName} <button class="del"></button> </span>`}
        }).join("");
    }
    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date()
}

const validate = (pattern, value) => pattern.test(value);


// видалення інгредієнтів 
// Не можу знайти картинки топігнів, щоб видалити їх з головного малюнка піци
function deleteIngredients(e) { 
    let delBtn = e.target.closest('.del')
    console.dir(delBtn)
    let parrent = delBtn.parentElement
    

    pizzaUser.topping.map(el => {
        if (el.quantity == 1) { 
            price.innerText -= el.price
            parrent.outerHTML = ''
            parrent.remove()
            pizzaUser.topping.splice(pizzaUser.topping.indexOf(el), 1);
            g = document.querySelector(`[data-name='${el.name}']`).outerHTML = '';
            // currentImg[currentImg.length-1].outerHTML = '';
        }
        if (parrent.innerText === el.productName && parrent.dataset.quantity > 1) { 
            el.quantity--
            parrent.dataset.quantity-- 
            price.innerText -= el.price
            console.log(table)
            let currentImg = document.querySelectorAll(`[data-name='${el.name}']`);
            console.dir(currentImg)
            currentImg[currentImg.length-1].outerHTML = '';
            if (parrent.dataset.quantity == 0) { 
                parrent.outerHTML = ''
                parrent.remove()
                pizzaUser.topping.splice(pizzaUser.topping.indexOf(el), 1)
                currentImg.outerHTML = '';
            }
        }
    })
}

//видалення соусів
function delSauce(e) {
    if (e.target.classList.contains('del-sauce')) {
        price.innerText -= pizzaUser.sauce.price;
        pizzaUser.sauce = '';
        sauce.innerHTML = ''
        document.querySelector('.sauce-img').innerHTML = '';

    }
}

const banner = document.querySelector('#banner')
function mouseover(event) { 
    banner.style.position = "absolute"
    banner.style.bottom = `${Math.floor(Math.random() * 50) + 1}%`
    banner.style.right = `${Math.floor(Math.random() * 80) + 1}%`
}



        table.addEventListener('dragover', (el) => {
            el.preventDefault()
        })

const [...draggable] = document.querySelectorAll('.draggable')
        draggable.forEach(item =>
            item.addEventListener('dragstart', (event) => {
            console.log(event)
                console.dir(event)
                
                event.dataTransfer.setData('ev', event.target.id)
                console.log(event.target.currentSrc)
                console.log(event.target)
            event.dataTransfer.effectAllowed = "move"

            }))

        table.addEventListener('drop', (event) => {
            let id = event.dataTransfer.getData('ev');
            console.log(id)
            let element = document.getElementById(id)
            let clone = element.cloneNode(true)
            console.log(clone)
            event.target.after(clone)
        })


/*
const [...draggable] = document.querySelectorAll('.draggable')
draggable.forEach(item => { 
    item.addEventListener('dragover', (el) => {
        console.log('+')
        el.preventDefault()
    })
        item.addEventListener('drop', (event) => {
        let id = event.dataTransfer.getData('ev');
            console.log(id)
            /*
        let element = document.getElementById(id)
        const div = document.createElement("div")
        div.innerText = "Hi"
        // target.appendChild(element)
        div.className = 'yellow-elemnt'
        target.appendChild(div)

    })
})

    table.addEventListener('dragstart', (event) => {
        event.dataTransfer.setData('ev', event.target.id)
        event.dataTransfer.effectAllowed = "move"
    })




/*
document.body.addEventListener('dragstart', function drag(elem) {
    if (elem.target.closest('.ingridients')) {
        let drag = elem.target

        table.addEventListener('dragleave', dragParts)
        function dragParts() {
            table.removeEventListener('dragleave', dragParts);
            pizzaSelectTopping(drag);
        }
    }
})
*/

export { pizzaSelectSize, pizzaSelectTopping, show, validate, deleteIngredients, mouseover, delSauce }