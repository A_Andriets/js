/*
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

const input = document.querySelector("#price");
const span = document.createElement('span');
const button = document.createElement('button');

// input.insertAdjacentHTML('afterend', '<button name="btn" id="btn">&#10060;</button>');
// const btn = document.querySelector('#btn');

input.addEventListener('focus', focus);
input.addEventListener('blur', blur);
input.addEventListener('change', change);
// window.addEventListener('DOMContentLoaded', () => {})
button.addEventListener('click', clickBtn)
 
// фокусування
function focus() { 
    console.log('+')
    input.classList.add('green')
    input.classList.remove('red')
} 

// расфокус
function blur() { 
    console.log('-');
    if (input.value < 0) { 
        console.log('red')
        input.classList.remove('green');
        input.classList.add('red');
        input.insertAdjacentHTML('afterend', '<span name="span" id="span">Please enter correct price</span>');
    } else {
        input.before(span);
        input.classList.remove('green');
        input.classList.remove('red');
        span.innerText = input.value;
        
    }
    // input.insertAdjacentHTML('afterend', '<button name="btn" id="btn">&#10060;</button>')
    input.after(button);
    button.innerText = '❌';
    
} 

// зміна стилю при введенні
function change() { 
    input.style.color = 'green';
    input.style.fontWeight = '700'
}

// клік по кнопці
function clickBtn() { 
    console.log('clear');
    span.remove();
    button.remove();
    input.value = '';
    input.removeAttribute("style")
    if (document.querySelector("#span").innerText == 'Please enter correct price') {
        console.log("delete")
        document.querySelector("#span").remove()
        input.classList.remove('red');
    }
};



